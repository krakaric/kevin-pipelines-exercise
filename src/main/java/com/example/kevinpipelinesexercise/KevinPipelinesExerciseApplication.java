package com.example.kevinpipelinesexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KevinPipelinesExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(KevinPipelinesExerciseApplication.class, args);
	}

}
